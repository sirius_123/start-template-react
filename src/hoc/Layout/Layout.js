import React, {Component} from 'react'
import classes from './Layout.css'
class Layout extends Component{
    state = {
        menu: false
    }
   /*  toggleMenuHandler = () => {
        this.setState({
            menu: !this.state.menu
        })
    }
    menuCloseHandler = () => {
        this.setState({
            menu: false
        })
    } */
    render() {
        return (
            <div className={classes.Layout}>
                <main>
                    {this.props.children}
                </main>
            </div>
        )
    }
}

export default Layout;

